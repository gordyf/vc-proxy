package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	mux "github.com/gorilla/mux"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

func main() {
	authKey := os.Getenv("AUTH_KEY")
	if authKey == "" {
		log.Fatal("AUTH_KEY not set")
		return
	}

	router := mux.NewRouter()

	router.HandleFunc("/set", vcHandler).Methods("POST").Headers("X-Auth-Key", authKey)

	srv := &http.Server{
		Handler:      router,
		Addr:         ":8080",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())
}

type dataRequest struct {
	Value string `json:"value"`
}

func vcHandler(w http.ResponseWriter, r *http.Request) {
	opts := mqtt.NewClientOptions().AddBroker("tcp://mqtt.gordyf.com:1883").SetClientID("vc-proxy")
	c := mqtt.NewClient(opts)
	if token := c.Connect(); token.Wait() && token.Error() != nil {
		http.Error(w, "MQTT error: "+token.Error().Error(), http.StatusBadRequest)
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "HTTP read error: "+err.Error(), http.StatusBadRequest)
		return
	}

	var data dataRequest
	json.Unmarshal(body, &data)

	token := c.Publish("vc/state", 0, false, data.Value)
	token.Wait()

	c.Disconnect(100)
}
